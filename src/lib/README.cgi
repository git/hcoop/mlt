Standard ML of New Jersey
README for contrib/lib/CGI

This directory contains SML translations of the cgi support modules
from the Moscow ML library.

These files compile under SML/NJ 110.0.3, but have not been tested.

----------------------------------------------------------------------

cgi.sig
cgi.sml
  Jonas Barklund, Computing Science Dept., Uppsala University, 1996
  Peter Sestoft <sestoft@dina.kvl.dk>
