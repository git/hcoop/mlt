(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Main CGI program interface *)

functor MainFn (Templates : TEMPLATES) :> MAIN =
struct
    val templates = foldl StringMap.insert' StringMap.empty Templates.templates

    fun main (_, args) =
	let
	    val _ = Cgi.init ()
	    val cgiFields = Cgi.cgi_fieldnames ()
	    fun mapper name = (name, Cgi.cgi_field_strings name)
	in
	    Web.addHeader ("Content-type", "text/html");
	    Web.pushParams (map mapper cgiFields);
	    Templates.beforeFn ();
	    case args of
		[] => (print "Status: 500\nContent-type: text/html\n\n<html><head><title>500 error</title></head><body><h2>500 error</h2>No template was specified</body></html>\n";
		       OS.Process.failure)
	      | (name::_) =>
		(case StringMap.find (templates, name) of
		     NONE => (print "Status: 404\nContent-type: text/html\n\n<html><head><title>404 error</title></head><body><h2>404 error</h2>Template not found</body></html>\n";
			      OS.Process.failure)
		   | SOME f => (f ();
				Templates.afterFn ();
				Web.output ();
				OS.Process.success))
	end handle ex => (Web.setExn ex;
			  Web.clear ();
			  (Templates.exnFn ();
			   if Web.noOutput () then
			       (print "Status: 500\nContent-type: text/plain\n\nUncaught exception:\n\n";
				app (fn s => print (s ^ "\n")) (SMLofNJ.exnHistory ex))
			   else
			       Web.output ())
			  handle ex' => (print "Status: 500\nContent-type : text/plain\n\nUncaught exception:\n\n";
					 app (fn s => print ("\t" ^ s ^ "\n")) (SMLofNJ.exnHistory ex);
					 print "\nAnother exception raised in exception handler:\n\n";
					 app (fn s => print ("\t" ^ s ^ "\n")) (SMLofNJ.exnHistory ex'));
			  OS.Process.failure)
end