structure StringKey :> ORD_KEY where type ord_key = string =
struct
    type ord_key = string
    val compare = String.compare
end

structure StringMap = BinaryMapFn(StringKey)
structure StringSet = BinarySetFn(StringKey)