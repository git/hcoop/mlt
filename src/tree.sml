(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Template language abstract syntax trees *)

structure Tree =
struct
    open Common

    type ident = string
    type path = ident list

    type 'a withext = 'a * (int * int)

    (* Pattern *)
    datatype pat' =
	Wild_p | Int_p of int | String_p of string | Char_p of string | Real_p of real
      | Ident_p of path
      | Record_p of bool * (ident * pat) list
      | FlexRecord_p of (ident * pat) list
      | App_p of path * pat | Cons_p of pat * pat
      | As_p of ident * pat
    and pat = PAT of pat' withext

    (* Expression *)
    and exp' =
	Int_e of int | String_e of string | Char_e of string | Real_e of real | Ident_e of path
      | Plus_e of exp * exp | Minus_e of exp * exp | Times_e of exp * exp
      | Divide_e of exp * exp | Mod_e of exp * exp | Neg_e | Param_e
      | Template_e of ident
      | Orelse_e of exp * exp | Andalso_e of exp * exp
      | StrCat_e of exp * exp
      | Eq_e of exp * exp | Neq_e of exp * exp
      | Gt_e of exp * exp | Gte_e of exp * exp
      | Lt_e of exp * exp | Lte_e of exp * exp
      | Cons_e of exp * exp | Compose_e of exp * exp
      | Record_e of bool * (ident * exp) list
      | RecordUpd_e of exp * (ident * exp) list
      | Proj_e of ident | App_e of exp * exp
      | Case_e of exp * (pat * exp) list
      | Fn_e of (pat * exp) list
      | Raise_e of exp
      | Let_e of block * exp
      | If_e of exp * exp * exp
    and exp = EXP of exp' withext

    and blockItem' =
	Html_i of string                                    (* literal HTML code to be sent *)
      | Ref_i of (ident * exp) list                         (* define some new refs *)
      | Val_i of pat * exp                                  (* val binding *)
      | Assn_i of ident * exp                               (* assignment to ref *)
      | Exp_i of exp                                        (* expression to be evaluated *)
      | Open_i of path list                                 (* imports to top level *)
      | Ifthenelse_i of exp * block * block option          (* if statement *)
      | Case_i of exp * (pat * block) list                  (* case statement *)
      | Foreach_i of pat * exp * block                    (* foreach statement with list *)
      | For_i of ident * exp * exp * block                  (* foreach statement with integer range *)
      | TryCatch_i of block * (pat * block) list            (* try...catch exception handlers w/ pattern matching *)
    and blockItem = BITEM of blockItem' withext
    (* Block of thingers to twiddle *)
    and block = BLOCK of block' withext
    withtype block' = blockItem list
    and path = ident list

    fun unblock (BLOCK (x, _)) = x
    fun unpat (PAT (x, _)) = x
    fun unexp (EXP (x, _)) = x


    fun expString (EXP (e, _)) =
	(case e of
	     Int_e n => Int.toString n
	   | String_e s => "\"" ^ s ^ "\""
	   | Ident_e [id] => id
	   | Ident_e [sn, id] => sn ^ "." ^ id
	   | Plus_e (e1, e2) => expString e1 ^ " + " ^ expString e2
	   | Minus_e (e1, e2) => expString e1 ^ " - " ^ expString e2
	   | Orelse_e (e1, e2) => expString e1 ^ " || " ^ expString e2
	   | Andalso_e (e1, e2) => expString e1 ^ " && " ^ expString e2
	   | Times_e (e1, e2) => expString e1 ^ " * " ^ expString e2
	   | Divide_e (e1, e2) => expString e1 ^ " / " ^ expString e2
	   | Mod_e (e1, e2) => expString e1 ^ " % " ^ expString e2
	   | Eq_e (e1, e2) => expString e1 ^ " = " ^ expString e2
	   | Neq_e (e1, e2) => expString e1 ^ " <> " ^ expString e2				
	   | Gt_e (e1, e2) => expString e1 ^ " > " ^ expString e2
	   | Gte_e (e1, e2) => expString e1 ^ " >= " ^ expString e2
	   | Lt_e (e1, e2) => expString e1 ^ " < " ^ expString e2
	   | Lte_e (e1, e2) => expString e1 ^ " <= " ^ expString e2
	   | Neg_e => "-"
	   | Param_e => "$"
	   | Template_e id => "@" ^ id
	   | Cons_e (e1, e2) => expString e1 ^ " :: " ^ expString e2
	   | Record_e (true, elms) => foldl (fn ((_, e), s) => s ^ ", " ^ expString e) "(" elms ^ ")"
	   | Record_e (false, elms) => foldl (fn ((lab, e), s) => s ^ ", " ^ lab ^ " = " ^ expString e) "(" elms ^ ")"
	   | RecordUpd_e _ => "<RECORDUPDATE>"
	   | Proj_e id => "#" ^ id
	   | App_e (e1, e2) => expString e1 ^ "(" ^ expString e2 ^ ")"
	   | _ => "<NUTTY>")

    fun pathString [] = "<ERROR>"
      | pathString [id] = id
      | pathString (h::t) = h ^ "." ^ pathString t
end