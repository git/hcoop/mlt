(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Command-line interface *)

structure Main =
struct
    fun main (_, args) =
	(case args of
	     [] => Compiler.compileDirectory (Config.read "mlt.conf" (Config.default ()))
	   (*| [path] => (Compiler.compileDirectory {inPath = path, outPath = path};
			OS.Process.success)
	   | [inPath, outPath] => (Compiler.compileDirectory {inPath = inPath, outPath = outPath};
				   OS.Process.success)*)
	   | _ => (print "Too many arguments.\n";
		   OS.Process.failure))
	handle Fail msg => (print "FATAL ERROR: ";
			    print msg;
			    print "\n";
			    OS.Process.failure)
	     | IO.Io {name, function, ...} => (print ("FATAL ERROR: Io: " ^ name ^ ": " ^ function ^ "\n");
			    OS.Process.failure)
end