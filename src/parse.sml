(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Template language parser *)

signature PARSE =
  sig
    val parse : string -> Tree.block
  end

structure Parse :> PARSE =
  struct 

    structure MltLrVals = MltLrValsFn(structure Token = LrParser.Token)
    structure Lex = MltLexFn(structure Tokens = MltLrVals.Tokens)
    structure MltP = Join(structure ParserData = MltLrVals.ParserData
                           structure Lex = Lex
                           structure LrParser = LrParser)

  (* The main parsing routine *)
  fun parse filename =
    let val _ = (ErrorMsg.reset(); ErrorMsg.fileName := filename)
	val file = TextIO.openIn filename
	fun get _ = TextIO.input file
	fun parseerror(s,p1,p2) = ErrorMsg.error (SOME (p1,p2)) s
	val lexer = LrParser.Stream.streamify (Lex.makeLexer get)
	val (absyn, _) = MltP.parse(30,lexer, parseerror, ())
    in
      TextIO.closeIn file;
      absyn
    end
      handle LrParser.ParseError => raise ErrorMsg.Error
end
