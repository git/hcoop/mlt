(*
 * Dynamic web page generation with Standard ML
 * Copyright (C) 2003  Adam Chlipala
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Lexing info for ML template language *)

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult = (svalue,pos) Tokens.token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos

fun strip s = String.extract (s, 1, SOME (String.size s - 2))

local
  val commentLevel = ref 0
  val commentPos = ref 0
  val linCom = ref false
in
  fun enterComment yypos = (commentLevel := !commentLevel + 1; commentPos := yypos)
    
  fun linComStart yypos = (linCom := true; commentPos := yypos)
  fun isLinCom () = !linCom
  fun linComEnd () = linCom := false

  fun exitComment () =
   let val _ = commentLevel := !commentLevel - 1 in
     !commentLevel = 0
   end

  fun eof () = 
    let 
      val pos = hd (!linePos)
    in
      if (!commentLevel > 0) then
          (ErrorMsg.error (SOME (!commentPos,!commentPos)) "Unterminated comment")
      else ();
      Tokens.EOF (pos,pos) 
    end
end

val str = ref ""
val strStart = ref 0

%%
%header (functor MltLexFn(structure Tokens : Mlt_TOKENS));
%full
%s COMMENT STRING CHAR CODE;

id = ([A-Za-z_][A-Za-z0-9_]*)|([:]+);
intconst = [0-9]+;
realconst = [0-9]+\.[0-9]*;
ws = [\ \t\012];
bo = [^<\n]+;

%%

<CODE> \n             => (if isLinCom () then (linComEnd (); YYBEGIN INITIAL) else ();
                          lineNum := !lineNum + 1;
                          linePos := yypos :: ! linePos;
                          continue ());
<INITIAL> \n          => (lineNum := !lineNum + 1;
                          linePos := yypos :: ! linePos;
                          Tokens.HTML (yytext, yypos, yypos + size yytext));

<INITIAL> {ws}+       => (Tokens.HTML (" ", yypos, yypos + size yytext); lex ());

<INITIAL> "<%"        => (YYBEGIN CODE; Tokens.SEMI(yypos, yypos + size yytext));
<CODE> "%>"           => (YYBEGIN INITIAL; Tokens.SEMI(yypos, yypos + size yytext));

<CODE> "(*"           => (YYBEGIN COMMENT; enterComment yypos; continue());
<CODE> "*)"           => (ErrorMsg.error (SOME (yypos, yypos)) "Unbalanced comments";
                             continue());

<COMMENT> "(*"        => (if not (isLinCom ()) then enterComment yypos else (); continue());
<COMMENT> "*)"        => (if not (isLinCom ()) andalso exitComment () then YYBEGIN INITIAL else ();
                              continue());

<CODE> "//"           => (YYBEGIN COMMENT; linComStart yypos; continue());

<CODE> {ws}+       => (lex ());

<CODE> "\""        => (YYBEGIN STRING; strStart := yypos; str := ""; continue());
<STRING> "\\\""    => (str := !str ^ "\\\""; continue());
<STRING> "\""      => (YYBEGIN CODE; Tokens.STRING (!str, !strStart, yypos + 1));
<STRING> .         => (str := !str ^ yytext; continue());

<CODE> "#\""       => (YYBEGIN CHAR; strStart := yypos; str := ""; continue());
<CHAR> "\\\""      => (str := !str ^ "\\\""; continue());
<CHAR> "\""        => (YYBEGIN CODE; if size (!str) = 1 then
					 Tokens.CHAR (!str, !strStart, yypos + 1)
				     else
					 (ErrorMsg.error (SOME (yypos, yypos)) "Invalid character constant";
					  continue()));
<CHAR> .           => (str := !str ^ yytext; continue());

<CODE> "{"         => (Tokens.LBRACE (yypos, yypos + size yytext));
<CODE> "}"         => (Tokens.RBRACE (yypos, yypos + size yytext));
<CODE> "("         => (Tokens.LPAREN (yypos, yypos + size yytext));
<CODE> ")"         => (Tokens.RPAREN (yypos, yypos + size yytext));
<CODE> "["         => (Tokens.LBRACK (yypos, yypos + size yytext));
<CODE> "]"         => (Tokens.RBRACK (yypos, yypos + size yytext));

<CODE> "="         => (Tokens.EQ (yypos, yypos + size yytext));
<CODE> "<>"        => (Tokens.NEQ (yypos, yypos + size yytext));
<CODE> "<"         => (Tokens.LT (yypos, yypos + size yytext));
<CODE> "<="        => (Tokens.LTE (yypos, yypos + size yytext));
<CODE> ">"         => (Tokens.GT (yypos, yypos + size yytext));
<CODE> ">="        => (Tokens.GTE (yypos, yypos + size yytext));

<CODE> ":="        => (Tokens.ASN (yypos, yypos + size yytext));

<CODE> "/"         => (Tokens.DIVIDE (yypos, yypos + size yytext));
<CODE> "*"         => (Tokens.TIMES (yypos, yypos + size yytext));
<CODE> "+"         => (Tokens.PLUS (yypos, yypos + size yytext));
<CODE> "-"         => (Tokens.MINUS (yypos, yypos + size yytext));
<CODE> "%"         => (Tokens.MOD (yypos, yypos + size yytext));
<CODE> "^"         => (Tokens.STRCAT (yypos, yypos + size yytext));

<CODE> "~"         => (Tokens.NEG (yypos, yypos + size yytext));
<CODE> ","         => (Tokens.COMMA (yypos, yypos + size yytext));
<CODE> ":"         => (Tokens.COLON (yypos, yypos + size yytext));
<CODE> "..."       => (Tokens.DOTDOTDOT (yypos, yypos + 3));
<CODE> ".."        => (Tokens.DOTDOT (yypos, yypos + 2));
<CODE> "."         => (Tokens.DOT (yypos, yypos + 1));
<CODE> "_"         => (Tokens.UNDER (yypos, yypos + 1));
<CODE> "#"         => (Tokens.HASH (yypos, yypos + 1));
<CODE> ";"         => (Tokens.SEMI (yypos, yypos + 1));
<CODE> "$"         => (Tokens.DOLLAR (yypos, yypos + size yytext));
<CODE> "@"         => (Tokens.AT (yypos, yypos + size yytext));

<CODE> "if"        => (Tokens.IF (yypos, yypos + 2));
<CODE> "iff"       => (Tokens.IFF (yypos, yypos + 3));
<CODE> "then"      => (Tokens.THEN (yypos, yypos + 4));
<CODE> "else"      => (Tokens.ELSE (yypos, yypos + 4));
<CODE> "elseif"    => (Tokens.ELSEIF (yypos, yypos + 6));
<CODE> "foreach"   => (Tokens.FOREACH (yypos, yypos + 7));
<CODE> "for"       => (Tokens.FOR (yypos, yypos + 3));
<CODE> "in"        => (Tokens.IN (yypos, yypos + 2));
<CODE> "case"      => (Tokens.CASE (yypos, yypos + 4));
<CODE> "as"        => (Tokens.AS (yypos, yypos + 2));
<CODE> "fn"        => (Tokens.FN (yypos, yypos + 2));
<CODE> "with"      => (Tokens.WITH (yypos, yypos + 4));
<CODE> "open"      => (Tokens.OPEN (yypos, yypos + 4));
<CODE> "val"       => (Tokens.VAL (yypos, yypos + 3));
<CODE> "ref"       => (Tokens.REF (yypos, yypos + 3));
<CODE> "try"       => (Tokens.TRY (yypos, yypos + 3));
<CODE> "catch"     => (Tokens.CATCH (yypos, yypos + 5));
<CODE> "or"        => (Tokens.ORELSE (yypos, yypos + 2));
<CODE> "and"       => (Tokens.ANDALSO (yypos, yypos + 3));
<CODE> "switch"    => (Tokens.SWITCH (yypos, yypos + 6));
<CODE> "of"        => (Tokens.OF (yypos, yypos + 2));
<CODE> "=>"        => (Tokens.ARROW (yypos, yypos + 2));
<CODE> "|"         => (Tokens.BAR (yypos, yypos + 1));
<CODE> "do"        => (Tokens.DO (yypos, yypos + 2));
<CODE> "end"       => (Tokens.END (yypos, yypos + 3));
<CODE> "raise"     => (Tokens.RAISE (yypos, yypos + 5));
<CODE> "let"       => (Tokens.LET (yypos, yypos + 3));
<CODE> "in"        => (Tokens.IN (yypos, yypos + 2));

<CODE> "::"        => (Tokens.CONS (yypos, yypos + 2));
<CODE> "o"         => (Tokens.O (yypos, yypos + 1));
<CODE> {id}        => (Tokens.IDENT (yytext, yypos, yypos + size yytext));
<CODE> {intconst}  => (case Int.fromString yytext of
                            SOME x => Tokens.INT (x, yypos, yypos + size yytext)
                          | NONE   => (ErrorMsg.error (SOME (yypos, yypos))
                                       ("Expected int, received: " ^ yytext);
                                       continue ()));
<CODE> {realconst} => (case Real.fromString yytext of
                            SOME x => Tokens.REAL (x, yypos, yypos + size yytext)
                          | NONE   => (ErrorMsg.error (SOME (yypos, yypos))
                                       ("Expected real, received: " ^ yytext);
                                       continue ()));

<CODE> "\"" {id} "\"" => (Tokens.STRING (String.substring(yytext, 1, String.size yytext - 2), yypos, yypos + size yytext));

<COMMENT> .           => (continue());

<INITIAL> {bo}        => (Tokens.HTML (yytext, yypos, yypos + size yytext));
<INITIAL> .           => (Tokens.HTML (yytext, yypos, yypos + 1));

<CODE> .              => (ErrorMsg.error (SOME (yypos,yypos))
                          ("illegal character: \"" ^ yytext ^ "\"");
                          continue ());
