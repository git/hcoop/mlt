(* This file comes mostly from "Modern Compiler Implementation in ML," by Andrew Appel
 * http://www.cs.princeton.edu/~appel/modern/ml/
 *)

signature ERRORMSG =
  sig
    val reset : unit -> unit
      
    val anyErrors : bool ref
    val errorText : string ref
      
    val fileName : string ref
    val sourceStream : TextIO.instream ref
      
    val lineNum : int ref
    val linePos : int list ref

    val error : (int * int) option -> string -> unit

    exception Error
end

structure ErrorMsg :> ERRORMSG =
  struct
    (* Initial values of compiler state variables *)
    val anyErrors = ref false
    val errorText = ref ""
    val fileName = ref ""
    val lineNum = ref 1
    val linePos = ref [1]
    val sourceStream = ref TextIO.stdIn

    fun print msg = (errorText := !errorText ^ msg;
		     TextIO.print msg)

    (* Reset compiler to initial state *)
    fun reset() = (anyErrors:=false;
		   errorText:="";
                   fileName:="";
                   lineNum:=1;
                   linePos:=[1];
                   sourceStream:=TextIO.stdIn)

    (* Print the given error message *)
    fun error posopt (msg:string) =
      let
        val (startpos, endpos) = Option.getOpt (posopt, (0, 0))
        fun look(pos,a::rest,n) =
          if a<pos then app print [Int.toString n,
                                   ".",
                                   Int.toString (pos-a)]
          else look(pos,rest,n-1)
          | look _ = print "0.0"
      in
        anyErrors := true;
        print (!fileName); print ":";
        look(startpos, !linePos, !lineNum);
        if startpos=endpos then () else (print "-"; look(endpos, !linePos, !lineNum));
        app print [":error: ", msg, "\n"]
      end

      exception Error
  end
